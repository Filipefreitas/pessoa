#ifndef  ALUNO_H
#define  ALUNO_H
#include "pessoa.h"

class Aluno: public Pessoa
{
	private:
		int matricula;
		int qtdcreditos;
		int semestre;
		float ira;
	public:
		Aluno();
		Aluno(string nome, string idade, string telefone, int matricula);
			void setMatricula(int matricula);
			int getMatricula();
			void setQtdCreditos(int qtdcreditos);
			int getQtdCreditos();
			void setSemestre(int semestre);
			int getSemestre();
			void setIra(float ira);
			float getIra();
};
#endif
